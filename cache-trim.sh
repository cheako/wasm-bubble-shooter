#!/bin/bash

: ${RUSTUP_HOME:=${HOME}/.rustup} ${CARGO_HOME:=${HOME}/.cargo}
(
    cd .cache/.rustup
    diff -qsra "$(dirname "$RUSTUP_HOME")/old_$(basename "$RUSTUP_HOME")" . | grep -v ^Only | cut -d\  -f4 | xargs rm -f
) &
(
    cd .cache/.cargo
    diff -qsra "$(dirname "$CARGO_HOME")/old_$(basename "$CARGO_HOME")" .   | grep -v ^Only |  cut -d\  -f4 | xargs rm -f
)
wait

find .cache/{.rustup,.cargo} -mount -print0 |
    perl -0 -ne 'chomp; my @s = stat($_); print "$s[8]/$s[9]/$_\0";' >.cache/dates.dat

