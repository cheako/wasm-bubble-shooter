//! Test suite for the Web and headless browsers.

#![cfg(target_arch = "wasm32")]

use wasm_bindgen_test::*;

wasm_bindgen_test_configure!(run_in_browser);

use wasm_bindgen::JsCast;
use web_sys::HtmlElement;

#[wasm_bindgen_test]
// #[ignore]
fn can_run() {
    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    let body = document.body().unwrap();
    let center = web_sys::DomParser::new()
        .unwrap()
        .parse_from_string(
            include_str!("../index.html"),
            web_sys::SupportedType::TextHtml,
        )
        .unwrap()
        .get_element_by_id("centerpoint")
        .unwrap();

    body.prepend_with_node_1(&center).unwrap();

    console_log!("{:?}", wasm_bubble_shooter::run());

    if let Some(child) = body.first_child() {
        body.remove_child(&child)
            .expect("body should remove fist child");
    }
}

#[wasm_bindgen_test]
fn can_fullscreen() {
    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    let body = document.body().unwrap();
    let center = web_sys::DomParser::new()
        .unwrap()
        .parse_from_string(
            include_str!("../index.html"),
            web_sys::SupportedType::TextHtml,
        )
        .unwrap()
        .get_element_by_id("centerpoint")
        .unwrap();

    body.prepend_with_node_1(&center).unwrap();

    wasm_bubble_shooter::run().unwrap();

    document
        .get_element_by_id("gofullscreen")
        .expect("should have #gofullscreen on the page")
        .dyn_ref::<HtmlElement>()
        .expect("#gofullscreen be an `HtmlElement`")
        .click();

    document.exit_fullscreen();
    if let Some(child) = body.first_child() {
        body.remove_child(&child)
            .expect("body should remove fist child");
    }
}

#[wasm_bindgen_test]
// #[ignore]
fn can_staticstart() {
    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    let body = document.body().unwrap();
    let center = web_sys::DomParser::new()
        .unwrap()
        .parse_from_string(
            include_str!("../index.html"),
            web_sys::SupportedType::TextHtml,
        )
        .unwrap()
        .get_element_by_id("centerpoint")
        .unwrap();

    body.prepend_with_node_1(&center).unwrap();

    wasm_bubble_shooter::run().unwrap();

    document
        .get_element_by_id("staticstart")
        .expect("should have #staticstart on the page")
        .dyn_ref::<HtmlElement>()
        .expect("#staticstart be an `HtmlElement`")
        .click();

    if let Some(child) = body.first_child() {
        body.remove_child(&child)
            .expect("body should remove fist child");
    }
}

#[wasm_bindgen_test]
fn can_shoot() {
    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    let body = document.body().unwrap();
    let center = web_sys::DomParser::new()
        .unwrap()
        .parse_from_string(
            include_str!("../index.html"),
            web_sys::SupportedType::TextHtml,
        )
        .unwrap()
        .get_element_by_id("centerpoint")
        .unwrap();

    body.prepend_with_node_1(&center).unwrap();

    wasm_bubble_shooter::run().unwrap();

    document
        .get_element_by_id("staticstart")
        .expect("should have #staticstart on the page")
        .dyn_ref::<HtmlElement>()
        .expect("#staticstart be an `HtmlElement`")
        .click();

    document
        .get_element_by_id("GameBoard")
        .expect("should have #GameBoard on the page")
        .dyn_ref::<HtmlElement>()
        .expect("#GameBoard be an `HtmlElement`")
        .click();

    if let Some(child) = body.first_child() {
        body.remove_child(&child)
            .expect("body should remove fist child");
    }
}
