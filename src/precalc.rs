use hex2d::Direction;

pub struct DataObject(
    /// The lowest coordinate on the tile.
    pub [f64; 2],
    /// The intersections to check.
    pub &'static[(Direction, [f64; 2])],
    /// The first coordinate above the tile.
    pub [f64; 2],
    /// The directions next tile is.
    pub Direction,
    /// The directions to check from next tile
    pub &'static[Direction],
    /// The new above height
    pub f64,
);
