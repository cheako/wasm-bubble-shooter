#![feature(cell_update)]
#![allow(non_snake_case)]

mod utils;

use js_sys::Function;
use rand::{rngs::OsRng, Rng};
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{
    CanvasRenderingContext2d, Document, HtmlCanvasElement, HtmlElement, HtmlImageElement,
    HtmlInputElement, MouseEvent, Touch, TouchEvent, Window,
};

// use hex2d::Spacing;
// use hex2d::Spacing::PointyTop;

use std::cell::{Cell, RefCell};
use std::f64::consts::PI;
use std::rc::Rc;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

// const HEXSPACEING: Spacing<f64> = PointyTop(40.);

#[derive(Debug, Default)]
struct Player {
    x: f64,
    y: f64,
    angle: f64,
    tiletype: u8,
    bubble_x: f64,
    bubble_y: f64,
    bubble_angle: f64,
    bubble_speed: f64,
    bubble_dropspeed: f64,
    bubble_tiletype: u8,
    bubble_visible: bool,
    nextbubble_x: f64,
    nextbubble_y: f64,
    nextbubble_tiletype: u8,
}

#[derive(Debug)]
struct TileImpl {
    x: usize,
    y: usize,
    type_: Option<u8>,
    removed: bool,
    shift: f64,
    velocity: f64,
    alpha: f64,
    processed: bool,
}
#[derive(Clone, Debug)]
struct Tile(Rc<RefCell<TileImpl>>);

#[derive(Debug, Default)]
struct Level {
    x: f64,
    y: f64,
    width: f64,
    height: f64,
    columns: usize, // Number of tile columns
    rows: usize,    // Number of tile rows
    tilesize: f64,  // Visual width of a tile
    rowheight: f64, // Height of a row
    radius: f64,    // Bubble collision radius
    scale: f64,     // This will be used all over to resize things
    dpr: f64,       // Mouse position adjust
    tiles: Vec<Vec<Tile>>,
}

thread_local! {
    static WINDOW: Window = web_sys::window().expect("should have a window in this context");
    static DOCUMENT: Document = WINDOW.with(|w|w.document().expect("window should have a document"));
    static BODY: HtmlElement = DOCUMENT.with(|d|d.body().expect("document should have a body"));
    static CANVAS: HtmlCanvasElement = { let ret: HtmlCanvasElement = DOCUMENT.with(|d|d .create_element("canvas") .expect("document should create the fullscreen canvas") .unchecked_into()); ret.set_id("GameBoard"); ret };
    static CONTEXT: CanvasRenderingContext2d = CANVAS.with(|c|c.get_context("2d")
                    .expect("canvas get context")
                    .expect("canvas have context")
                    .unchecked_into());
    static LEVEL: RefCell<Level> = Default::default();
    static SPEED: Cell<f64> = Default::default();
}

fn withLevel<F, R>(f: F) -> R
where
    F: FnOnce(&Level) -> R,
{
    LEVEL.with(|level| f(&level.borrow()))
}

fn withMutLevel<F, R>(f: F) -> R
where
    F: FnOnce(&mut Level) -> R,
{
    LEVEL.with(|level| f(&mut level.borrow_mut()))
}

thread_local! {
    static FULLSCREEN: RefCell<Option<JsValue>> =
        RefCell::new(Some(Closure::once_into_js(move ||
                DOCUMENT.with(|document|
                BODY.with(|body|
                CANVAS.with(|canvas|
                CONTEXT.with(|context|{

                let speed = document
                    .get_element_by_id("speed")
                    .expect("should have #speed on the page")
                    .dyn_ref::<HtmlInputElement>()
                    .expect("#speed be an `HtmlInputElement`")
                    .value()
                    .parse::<f64>()
                    .expect("#speed value number");

                let tilesize = document
                    .get_element_by_id("tilesize")
                    .expect("should have #tilesize on the page")
                    .dyn_ref::<HtmlInputElement>()
                    .expect("#tilesize be an `HtmlInputElement`")
                    .value()
                    .parse::<f64>()
                    .expect("#tilesize value number");

                let scale = tilesize / 40.;
                let rowheight = 34. * scale;

                if let Some(child) = body.first_child() {
                    body.remove_child(&child)
                        .expect("body should remove fist child");
                }
                clear_init_hooks();
                body.prepend_with_node_1(&canvas)
                    .expect("body should append canvas");

                canvas
                    .request_fullscreen()
                    .expect("canvas should go fullscreen");

                let dpr = WINDOW.with(|w|w.device_pixel_ratio());
                let rect = canvas.get_bounding_client_rect();
                canvas.set_width((rect.width() * dpr) as u32);
                canvas.set_height((rect.height() * dpr) as u32);

                let width = rect.width() - 8. * scale;
                // No idea where 66 comes from.
                let height = rect.height() - 2. * tilesize - 66. * scale;

                // Level
                LEVEL.with(|level|
                    level.replace(Level {
                        x: 4. * scale,                                                // X position
                        y: 83. * scale,                                               // Y position
                        width,                                                        // Width
                        height,                                                       // Height
                        columns: (width / tilesize - 0.5).floor() as usize, // Number of tile columns
                        rows: ((height - tilesize) / rowheight).floor() as usize + 1, // Number of tile rows
                        tilesize: tilesize,    // Visual width of a tile
                        rowheight: rowheight,  // Height of a row
                        radius: tilesize / 2., // Bubble collision radius
                        scale: scale,          // This will be used all over to resize things
                        dpr: dpr,              // Mouse position adjust
                        tiles: vec![],         // The two-dimensional tile array
                }));

                context.scale(dpr, dpr).expect("context to scale");

                init(speed);})))))));

    static STATICSTART: RefCell<Option<JsValue>> =
                RefCell::new(Some(Closure::once_into_js(move ||
                DOCUMENT.with(|document|
                CANVAS.with(|canvas|
                CONTEXT.with(|context|{

                let speed = document
                    .get_element_by_id("speed")
                    .expect("should have #speed on the page")
                    .dyn_ref::<HtmlInputElement>()
                    .expect("#speed be an `HtmlInputElement`")
                    .value()
                    .parse::<f64>()
                    .expect("#speed value number");

                let tilesize = document
                    .get_element_by_id("tilesize")
                    .expect("should have #tilesize on the page")
                    .dyn_into::<HtmlInputElement>()
                    .expect("#tilesize be an `HtmlInputElement`")
                    .value()
                    .parse::<f64>()
                    .expect("#tilesize value should be number");

                let scale = tilesize / 40.;
                let rowheight = 34. * scale;
                let columns = document
                    .get_element_by_id("columns")
                    .expect("should have #columns on the page")
                    .dyn_into::<HtmlInputElement>()
                    .expect("#columns be an `HtmlInputElement`")
                    .value()
                    .parse::<usize>()
                    .expect("#columns value should be number");
                let rows = document
                    .get_element_by_id("rows")
                    .expect("should have #rows on the page")
                    .dyn_into::<HtmlInputElement>()
                    .expect("#rows be an `HtmlInputElement`")
                    .value()
                    .parse::<usize>()
                    .expect("#rows value should be number");
                let dpr = WINDOW.with(|w|w.device_pixel_ratio());

                // Level
                let level = Level {
                    x: 4. * scale,                                    // X position
                    y: 83. * scale,                                   // Y position
                    width: (columns as f64 + 0.5) * tilesize, // Width
                    height: rows as f64 * rowheight + tilesize,       // Height
                    columns,                                          // Number of tile columns
                    rows: rows + 1,                                   // Number of tile rows
                    tilesize: tilesize,                               // Visual size of a tile
                    rowheight: rowheight,                             // Height of a row
                    radius: tilesize / 2.,                            // Bubble collision radius
                    scale: scale,  // This will be used all over to resize things
                    dpr: dpr,      // Mouse position adjust
                    tiles: vec![], // The two-dimensional tile array
                };

                let width = level.width + 8. * scale;
                // No idea where 66 comes from.
                let height = level.height + tilesize + tilesize + 66. * scale;

                LEVEL.with(|l| l.replace(level));

                canvas.set_width((width * dpr) as u32);
                canvas.set_height((height * dpr) as u32);
                let style = canvas.style();
                style.set_property("width", &format!("{}px", width)).expect("to set canvas style width");
                style.set_property("height", &format!("{}px", height)).expect("to set canvas style height");
                style.set_property("z-index", "8").expect("to set canvas style zIndex");
                style.set_property("position", "absolute").expect("to set canvas style position");
                style.set_property("border", "1px solid").expect("to set canvas style border");
                style.set_property("margin-left", &format!("{}px", (width + 2.) / -2.)).expect("to set canvas style marginLeft");
                style.set_property("margin-top", &format!("{}px", (height + 2.) / -2.)).expect("to set canvas style marginTop");

                let center = document
                    .get_element_by_id("centerpoint")
                    .expect("should have #centerpoint on the page")
                    .dyn_into::<HtmlElement>()
                    .expect("#centerpoint be an `HtmElement`");

                while let Some(child) = center.first_child() {
                    center.remove_child(&child)
                        .expect("body should remove fist child");
                }
                clear_init_hooks();
                center
                    .prepend_with_node_1(&canvas)
                    .expect("body should append canvas");

                context.scale(dpr, dpr).expect("context to scale");

                init(speed);}))))));
}

fn clear_init_hooks() {
    FULLSCREEN.with(|fullscreen| fullscreen.replace(None));
    STATICSTART.with(|staticstart| staticstart.replace(None));
}

// Called by our JS entry point to run the example
#[wasm_bindgen(start)]
pub fn run() -> Result<(), JsValue> {
    utils::set_panic_hook();
    DOCUMENT.with(|document| {
        FULLSCREEN.with(|fullscreen| {
            document
                .get_element_by_id("gofullscreen")
                .expect("should have #gofullscreen on the page")
                .dyn_ref::<HtmlElement>()
                .expect("#gofullscreen be an `HtmlElement`")
                .set_onclick(Some(
                    fullscreen
                        .borrow()
                        .as_ref()
                        .unwrap()
                        .unchecked_ref::<Function>(),
                ))
        });
        STATICSTART.with(|staticstart| {
            document
                .get_element_by_id("staticstart")
                .expect("should have #staticstart on the page")
                .dyn_ref::<HtmlElement>()
                .expect("#staticstart be an `HtmlElement`")
                .set_onclick(Some(
                    staticstart
                        .borrow()
                        .as_ref()
                        .unwrap()
                        .unchecked_ref::<Function>(),
                ))
        });
        Ok(())
    })
}

impl Tile {
    fn new(x: usize, y: usize, type_: u8, shift: f64) -> Self {
        Self(Rc::new(RefCell::new(TileImpl {
            x,
            y,
            type_: Some(type_),
            removed: false,
            shift,
            velocity: 0.,
            alpha: 1.,
            processed: false,
        })))
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
enum GameStates {
    INIT,
    READY,
    SHOOTBUBBLE,
    REMOVECLUSTER,
    GAMEOVER,
}

fn getGameState() -> GameStates {
    GAMESTATE.with(|g| g.get())
}

thread_local! {
    static IMAGEONLOAD: RefCell<Option<JsValue>> = RefCell::new(Some(Closure::once_into_js(move ||
        LOADTOTAL.with(|loadtotal|
        LOADCOUNT.with(|loadcount| {
            if loadcount.update(|x|x+1) == loadtotal.get() {
                // Done loading
                PRELOADED.with(|p|p.set(true));
            }
    })))));

    // Timing and frames per second
    static LASTFRAME: Cell<f64> = Cell::new(0.);
    static FPSTIME: Cell<f64> = Cell::new(0.);
    static FPSCOUNT: Cell<u128> = Cell::new(0);
    static FPS: Cell<f64> = Cell::new(0.);

    // Player
    static PLAYER: RefCell<Player> = Default::default();

    // Neighbor offset table
    static NEIGHBORSOFFSETS: [[[i32; 2]; 6]; 2] = [
        [[1, 0], [0, 1], [-1, 1], [-1, 0], [-1, -1], [0, -1]], // Even row tiles
        [[1, 0], [1, 1], [0, 1], [-1, 0], [0, -1], [1, -1]],
    ]; // Odd row tiles

    // Number of different colors
    static BUBBLECOLORS: u8 = 7;

    // Game states
    static GAMESTATE: Cell<GameStates> = Cell::new(GameStates::INIT);

    // Score
    static SCORE: Cell<u128> = Cell::new(0);

    static TURNCOUNTER: Cell<u32> = Cell::new(0);
    static ROWOFFSET: Cell<usize> = Cell::new(0);

    // Animation variables
    static ANIMATIONSTATE: Cell<u32> = Cell::new(0);
    static ANIMATIONTIME: Cell<u32> = Cell::new(0);

    // Clusters
    static SHOWCLUSTER: Cell<bool> = Cell::new(false);
    static CLUSTER: RefCell<Vec<Tile>> = Default::default();
    static FLOATINGCLUSTERS: RefCell<Vec<Vec<Tile>>> = RefCell::new(vec![]);

    // Touch
    static TOUCH_IDENTIFYER: Cell<i32> = Cell::new(0);
    static TOUCH_TIMESTAMP: Cell<f64> = Cell::new(0.);

    // Leaderboard
    static SCORINGFLAGS: () = ();

    // Images
    static IMAGES: RefCell<Vec<HtmlImageElement>> = RefCell::new(vec![]);
    static BUBBLEIMAGE: RefCell<Option<HtmlImageElement>> = RefCell::new(None);

    // Image loading global variables
    static LOADCOUNT: Cell<usize> = Cell::new(0);
    static LOADTOTAL: Cell<usize> = Cell::new(0);
    static PRELOADED: Cell<bool> = Cell::new(false);
}

fn withPlayer<F, R>(f: F) -> R
where
    F: FnOnce(&Player) -> R,
{
    PLAYER.with(|player| f(&player.borrow()))
}

fn withMutPlayer<F, R>(f: F) -> R
where
    F: FnOnce(&mut Player) -> R,
{
    PLAYER.with(|player| f(&mut player.borrow_mut()))
}

fn withLevelMutPlayer<F, R>(f: F) -> R
where
    F: FnOnce(&Level, &mut Player) -> R,
{
    withLevel(|level| withMutPlayer(|player| f(level, player)))
}

fn withMutLevelMutPlayer<F, R>(f: F) -> R
where
    F: FnOnce(&mut Level, &mut Player) -> R,
{
    withMutLevel(|level| withMutPlayer(|player| f(level, player)))
}

fn loadImages(imagefiles: &[&str]) -> Vec<HtmlImageElement> {
    LOADTOTAL.with(|loadtotal| loadtotal.set(imagefiles.len()));

    // Load the images
    let mut loadedimages = vec![];
    for i in 0..imagefiles.len() {
        // Create the image object
        let image = HtmlImageElement::new().expect("create new image");

        // Add onload event handler
        IMAGEONLOAD.with(|f| image.set_onload(Some(f.borrow().as_ref().unwrap().unchecked_ref())));

        // Set the source url of the image
        image.set_src(imagefiles[i]);

        // Save to the image array
        loadedimages.push(image);
    }

    // Return an array of images
    loadedimages
}

// Initialize the game
fn init(speed: f64) {
    CANVAS.with(|canvas| {
        // Load images
        IMAGES.with(|images| {
            BUBBLEIMAGE.with(|bubbleimage| {
                images.replace(loadImages(std::slice::from_ref(&"bubble-sprites.png")));
                bubbleimage.replace(Some(images.borrow()[0].clone()));
            });
        });

        // Add mouse events
        let closure =
            Closure::wrap(Box::new(move |e: MouseEvent| onMouseMove(e)) as Box<dyn Fn(_)>);
        canvas
            .add_event_listener_with_callback("mousemove", closure.as_ref().unchecked_ref())
            .expect("canvas add mousemove listener");
        closure.forget();
        let closure =
            Closure::wrap(Box::new(move |e: MouseEvent| onMouseDown(e)) as Box<dyn Fn(_)>);
        canvas
            .add_event_listener_with_callback("mousedown", closure.as_ref().unchecked_ref())
            .expect("canvas add mousemove listener");
        closure.forget();

        // Add touch events
        let closure =
            Closure::wrap(Box::new(move |e: TouchEvent| touchHandleStart(e)) as Box<dyn Fn(_)>);
        canvas
            .add_event_listener_with_callback("touchstart", closure.as_ref().unchecked_ref())
            .expect("canvas add mousemove listener");
        closure.forget();
        let closure =
            Closure::wrap(Box::new(move |e: TouchEvent| touchHandleMove(e)) as Box<dyn Fn(_)>);
        canvas
            .add_event_listener_with_callback("touchmove", closure.as_ref().unchecked_ref())
            .expect("canvas add mousemove listener");
        closure.forget();
        let closure =
            Closure::wrap(Box::new(move |e: TouchEvent| touchHandleEnd(e)) as Box<dyn Fn(_)>);
        canvas
            .add_event_listener_with_callback("touchend", closure.as_ref().unchecked_ref())
            .expect("canvas add mousemove listener");
        closure.forget();
        let closure =
            Closure::wrap(Box::new(move |e: TouchEvent| touchHandleCancel(e)) as Box<dyn Fn(_)>);
        canvas
            .add_event_listener_with_callback("touchcancel", closure.as_ref().unchecked_ref())
            .expect("canvas add mousemove listener");
        closure.forget();

        // Initialize the two-dimensional tile array
        withMutLevelMutPlayer(|level, player| {
            for i in 0..level.columns as usize {
                level.tiles.push(vec![]);
                for j in 0..level.rows as usize {
                    // Define a tile type and a shift parameter for animation
                    level.tiles[i].push(Tile::new(i, j, 0, 0.));
                }
            }

            // Init the player
            player.x = level.x + level.width / 2.;
            player.y = level.y + level.height + level.tilesize / 2.;
            player.angle = PI;
            player.tiletype = 0;

            player.bubble_speed = speed;
            player.bubble_dropspeed = speed * 1000. / 900.;

            player.nextbubble_x = player.x - level.tilesize - level.tilesize;
            player.nextbubble_y = player.y;
        });

        // New game
        newGame();

        // Enter main loop
        preloader(0.);
    })
}

thread_local! {
    static PRELOADER: Closure<dyn Fn(f64)> = Closure::wrap(Box::new(move |t: f64| preloader(t)));
    static MAIN: Closure<dyn Fn(f64)> = Closure::wrap(Box::new(move |t: f64| main(t)));
}

fn preloader(_tframe: f64) {
    WINDOW.with(|window| {
        CANVAS.with(|canvas| {
            CONTEXT.with(|context| {
                PRELOADER.with(|preloader| {
                    MAIN.with(|main| {
                        // Request animation frames
                        window
                            .request_animation_frame(if PRELOADED.with(|p| p.get()) {
                                IMAGEONLOAD.with(|f| f.replace(None));
                                main.as_ref().unchecked_ref()
                            } else {
                                preloader.as_ref().unchecked_ref()
                            })
                            .expect("window should have preload animation frame");
                    })
                });

                // Clear the canvas
                context.clear_rect(0., 0., canvas.width() as _, canvas.height() as _);

                LOADCOUNT.with(|loadcount| {
                    LOADTOTAL.with(|loadtotal| {
                        withLevel(|level| {
                            // Draw the frame
                            drawFrame(level.scale);

                            // Draw a progress bar
                            let loadpercentage = loadcount.get() / loadtotal.get();
                            context.set_stroke_style(&JsValue::from_str("#ff8080"));
                            context.set_line_width(3. * level.scale);
                            context.stroke_rect(
                                18.5 * level.scale,
                                canvas.height() as f64 - 50.5 * level.scale,
                                canvas.width() as f64 - 37. * level.scale,
                                32. * level.scale,
                            );
                            context.set_fill_style(&JsValue::from_str("#ff8080"));
                            context.fill_rect(
                                18.5 * level.scale,
                                canvas.height() as f64 - 50.5 * level.scale,
                                loadpercentage as f64 * (canvas.width() as f64 - 37. * level.scale),
                                32. * level.scale,
                            );

                            // Draw the progress text
                            let loadtext =
                                format!("Loaded {}/{} images", loadcount.get(), loadtotal.get());
                            context.set_fill_style(&JsValue::from_str("#000000"));
                            context.set_font(&format!("{}px Verdana", 16. * level.scale));
                            context
                                .fill_text(
                                    &loadtext,
                                    18. * level.scale,
                                    canvas.height() as f64 - 62.5 * level.scale,
                                )
                                .expect("context fill load text");
                        })
                    })
                })
            })
        })
    })
}

// Main loop
fn main(tframe: f64) {
    WINDOW.with(|window|
        // Request animation frames
        MAIN.with(|main| window.request_animation_frame(main.as_ref().unchecked_ref()).expect("window should have main animation frame")));

    // Update and render the game
    let dt = LASTFRAME.with(move |lastframe| {
        let dt = (tframe - lastframe.get()) / 1000.;
        lastframe.set(tframe);
        dt
    });

    // Update the fps counter
    updateFps(dt);

    match getGameState() {
        GameStates::READY => {
            // Game is ready for player input
        }
        GameStates::SHOOTBUBBLE => {
            // Bubble is moving
            stateShootBubble(dt);
        }
        GameStates::REMOVECLUSTER => {
            // Remove cluster and drop tiles
            stateRemoveCluster(dt);
        }
        _ => {}
    };

    render();
}

fn setGameState(newgamestate: GameStates) {
    GAMESTATE.with(move |g| g.set(newgamestate));

    ANIMATIONSTATE.with(|a| a.set(0));
    ANIMATIONTIME.with(|a| a.set(0));
}

fn stateShootBubble(dt: f64) {
    withMutLevelMutPlayer(|level, player| {
        // Bubble is moving
        let dist: f64 = dt * player.bubble_speed;
        let deltax = dist * player.bubble_angle.cos();
        let deltay = dist * player.bubble_angle.sin();
        let number_movements = (dist / level.radius).ceil();
        let rightEdge = level.x + level.width - level.tilesize / 2.;
        let leftEdge = level.x + level.tilesize / 2.;

        for _ in 0..number_movements as u32 {
            // Move the bubble in the direction of the mouse
            player.bubble_x += deltax / number_movements;
            player.bubble_y -= deltay / number_movements;

            // Handle left and right collisions with the level
            if player.bubble_x <= leftEdge {
                // Left edge
                player.bubble_angle = PI - player.bubble_angle;
                player.bubble_x = leftEdge * 2. - player.bubble_x;
            } else if player.bubble_x >= rightEdge {
                // Right edge
                player.bubble_angle = PI - player.bubble_angle;
                player.bubble_x = rightEdge * 2. - player.bubble_x;
            }

            // Collisions with the top of the level
            if player.bubble_y <= level.y {
                // Top collision
                let ydelta = level.y - player.bubble_y;
                player.bubble_x += player.bubble_angle.sin() * player.bubble_angle.cos() / ydelta;
                snapBubble(level, player);
                return;
            }

            // let gridpos = getPlayerGridPosition();

            /*
            let lookup = [{
                i: gridpos.x /* minus or plus one? */ ,
                j: gridpos.y + 1
            }, {
                i: gridpos.x /* minus or plus one? */ ,
                j: gridpos.y + 1
            }, {
                i: gridpos.x - 1,
                j: gridpos.y
            }, {
                i: gridpos.x + 1,
                j: gridpos.y
            }, {
                i: gridpos.x /* minus or plus one? */ ,
                j: gridpos.y - 1
            }, {
                i: gridpos.x /* minus or plus one? */ ,
                j: gridpos.y - 1
            }];
            let things = [{
                v: PI / 4,
                e: () => {
                    return [lookup[1], lookup[3], lookup[5]]
                }
            }, {
                v: player.angle,
                e: () => {
                    if (player.angle == PI / 2) {
                        return [lookup[0], lookup[1]];
                    } else {
                        return [lookup[0], lookup[1], player.angle < PI / 2 ? lookup[2] : lookup[3]];
                    }
                }
            }, {
                v: PI * 3. / 4.,
                e: () => {
                    return [lookup[0], lookup[2], lookup[4]];
                }
            }].sort(|a, b|
                a.v - b.v
            )[1].e(); */

            // Collisions with other tiles
            for i in 0..level.columns {
                for j in 0..level.rows {
                    // Skip empty tiles
                    if level.tiles[i][j].0.borrow().type_.is_none() {
                        continue;
                    }

                    // Check for intersections
                    let coord = getTileCoordinate(level, i, j);
                    if
                    // Check if two circles intersect
                    {
                        let f = move |x1, y1, r1, x2, y2, r2| {
                            // Calculate the distance between the centers
                            let dx = x1 - x2;
                            let dy = y1 - y2;

                            dx * dx + dy * dy < r1 * r1 + r2 * r2
                        };
                        f(
                            player.bubble_x,
                            player.bubble_y,
                            level.radius,
                            coord[0],
                            coord[1],
                            level.radius,
                        )
                    } {
                        // Intersection with a level bubble
                        snapBubble(level, player);
                        return;
                    }
                }
            }
        }
    })
}

fn stateRemoveCluster(dt: f64) {
    ANIMATIONSTATE.with(|animationstate| {
        CLUSTER.with(|cluster| {
            if animationstate.get() == 0 {
                let mut cluster = cluster.borrow_mut();
                resetRemoved();

                // Mark the tiles as removed
                for i in 0..cluster.len() {
                    // Set the removed flag
                    cluster[i].0.borrow_mut().removed = true;
                }

                SCORE.with(|score| {
                    FLOATINGCLUSTERS.with(|floatingclusters| {
                        // Add cluster score
                        score.update(|s| s + cluster.len() as u128 * 100);

                        // Find floating clusters
                        floatingclusters.replace(findFloatingClusters());

                        let floatingclusters = floatingclusters.borrow();
                        if !floatingclusters.is_empty() {
                            // Setup drop animation
                            for i in 0..floatingclusters.len() {
                                for j in 0..floatingclusters[i].len() {
                                    let mut tile = floatingclusters[i][j].0.borrow_mut();
                                    /* TODO: This is a bug in our upstream, should set angle too. */
                                    tile.shift = 1.;
                                    tile.velocity = withPlayer(|player| player.bubble_dropspeed);

                                    score.update(|s| s + 100);
                                }
                            }
                        }
                    })
                });
                animationstate.set(1);
            }

            if animationstate.get() == 1 {
                // Pop bubbles
                let mut tilesleft = false;
                let cluster = cluster.borrow();
                for i in 0..cluster.len() {
                    let mut tile = cluster[i].0.borrow_mut();

                    if tile.type_.is_some() {
                        tilesleft = true;

                        // Alpha animation
                        tile.alpha -= dt * 15.;
                        if tile.alpha < 0. {
                            tile.alpha = 0.;
                        }

                        if tile.alpha == 0. {
                            tile.type_ = None;
                            tile.alpha = 1.;
                        }
                    }
                }

                FLOATINGCLUSTERS.with(|floatingclusters| {
                    // Drop bubbles
                    let floatingclusters = floatingclusters.borrow();
                    for i in 0..floatingclusters.len() {
                        for _ in 0..floatingclusters[i].len() {
                            let mut tile = cluster[i].0.borrow_mut();

                            if tile.type_.is_some() {
                                tilesleft = true;

                                // Accelerate dropped tiles
                                tile.velocity += dt * 700.;
                                tile.shift += dt * tile.velocity;
                                // Alpha animation
                                tile.alpha -= dt * 8.;
                                if tile.alpha < 0. {
                                    tile.alpha = 0.;
                                }

                                withLevel(|level| {
                                    // Check if the bubbles are past the bottom of the level
                                    if tile.alpha == 0.
                                        || (tile.y as f64 * level.rowheight + tile.shift
                                            > (level.rows - 1) as f64 * level.rowheight
                                                + level.tilesize)
                                    {
                                        tile.type_ = None;
                                        tile.shift = 0.;
                                        tile.alpha = 1.;
                                    }
                                })
                            }
                        }
                    }
                });

                if !tilesleft {
                    withLevelMutPlayer(|level, player| {
                        // Next bubble
                        nextBubble(level, player);

                        // Check for game over
                        let mut tilefound = false;
                        for i in 0..level.columns {
                            for j in 0..level.rows {
                                /* TODO, check this?? */
                                if level.tiles[i][j].0.borrow().type_.is_none() {
                                    tilefound = true;
                                    break;
                                }
                            }
                        }

                        setGameState(if tilefound {
                            GameStates::READY
                        } else {
                            // No tiles left, game over
                            GameStates::GAMEOVER
                        });
                    })
                }
            }
        })
    })
}

// Snap bubble to the grid
fn snapBubble(level: &mut Level, player: &mut Player) {
    TURNCOUNTER.with(|turncounter| {
        CLUSTER.with(|cluster| {
            // Get the grid position
            let mut gridpos = getPlayerGridPosition(level, player);

            // Make sure the grid position is valid
            if gridpos[0] >= level.columns {
                gridpos[0] = level.columns - 1;
            }

            if gridpos[1] >= level.rows {
                gridpos[1] = level.rows - 1;
            }

            // Check if the tile is empty
            let mut addtile = false;
            if level.tiles[gridpos[0]][gridpos[1]]
                .0
                .borrow()
                .type_
                .is_some()
            {
                // Tile is not empty, shift the new tile downwards
                for newrow in gridpos[1] + 1..level.rows {
                    if level.tiles[gridpos[0]][newrow].0.borrow().type_.is_none() {
                        gridpos[1] = newrow;
                        addtile = true;
                        break;
                    }
                }
            } else {
                addtile = true;
            }

            // Add the tile to the grid
            if addtile {
                // Hide the player bubble
                player.bubble_visible = false;

                // Set the tile, this is where level is mut
                level.tiles[gridpos[0]][gridpos[1]].0.borrow_mut().type_ =
                    Some(player.bubble_tiletype);

                // Check for game over
                if checkGameOver(level) {
                    return;
                }

                // Find clusters
                cluster.replace(findCluster(
                    level, gridpos[0], gridpos[1], true, true, false,
                ));

                if cluster.borrow().len() >= 3 {
                    // Remove the cluster
                    setGameState(GameStates::REMOVECLUSTER);
                    return;
                }
            }
            // No clusters found
            if turncounter.update(|t| t + 1) >= 5 {
                // Add a row of bubbles
                addBubbles();
                turncounter.set(0);
                ROWOFFSET.with(|rowoffset| rowoffset.update(|r| (r + 1) % 2));

                if checkGameOver(level) {
                    return;
                }
            }

            // Next bubble
            nextBubble(level, player);
            setGameState(GameStates::READY);
        })
    })
}

fn checkGameOver(level: &Level) -> bool {
    // Check for game over
    for i in 0..level.columns {
        // Check if there are bubbles in the bottom row
        if level.tiles[i][level.rows - 1].0.borrow().type_.is_some() {
            // Game over
            withMutPlayer(|player| {
                nextBubble(level, player);
            });
            setGameState(GameStates::GAMEOVER);
            return true;
        }
    }

    false
}

fn addBubbles() {
    withMutLevel(|level| {
        let rows = level.rows;

        // Move the rows downwards
        for i in 0..level.columns {
            for j in 0..level.rows - 1 {
                let mut tilea = level.tiles[i][rows - 1 - j].0.borrow_mut();
                let tileb = level.tiles[i][rows - 1 - j - 1].0.borrow();
                tilea.type_ = tileb.type_;
            }
        }

        // Add a new row of bubbles at the top
        for i in 0..level.columns {
            // Add random, existing, colors
            level.tiles[i][0].0.borrow_mut().type_ = Some(getExistingColor(level));
        }
    })
}

// Find the remaining colors
fn findColors(level: &Level) -> Vec<u8> {
    BUBBLECOLORS.with(|bubblecolors| {
        let mut foundcolors = vec![];
        let mut colortable = vec![];
        for _ in 0..*bubblecolors {
            colortable.push(false);
        }

        // Check all tiles
        for i in 0..level.columns {
            for j in 0..level.rows {
                if let Some(type_) = level.tiles[i][j].0.borrow().type_ {
                    if !colortable[type_ as usize] {
                        colortable[type_ as usize] = true;
                        foundcolors.push(type_);
                    }
                }
            }
        }

        foundcolors
    })
}

// Find cluster at the specified tile location
fn findCluster(
    level: &mut Level,
    tx: usize,
    ty: usize,
    matchtype: bool,
    reset: bool,
    skipremoved: bool,
) -> Vec<Tile> {
    // Reset the processed flags
    if reset {
        resetProcessed(level);
    }

    // Get the target tile. Tile coord must be valid.
    let targettile = level.tiles[tx][ty].clone();

    // Initialize the toprocess array with the specified tile
    let mut toprocess = vec![targettile.clone()];
    targettile.0.borrow_mut().processed = true;
    let mut foundcluster = vec![];

    // Pop the last element from the array
    while let Some(currenttile) = toprocess.pop() {
        // Skip processed and empty tiles
        if currenttile.0.borrow().type_.is_none() {
            continue;
        }

        // Skip tiles with the removed flag
        if skipremoved && currenttile.0.borrow().removed {
            continue;
        }

        // Check if current tile has the right type, if matchtype is true
        if !matchtype || (currenttile.0.borrow().type_ == targettile.0.borrow().type_) {
            // Add current tile to the cluster
            foundcluster.push(currenttile.clone());

            // Get the neighbors of the current tile
            let mut neighbors = getNeighbors(level, currenttile);

            // Check the type of each neighbor
            for i in 0..neighbors.len() {
                if !neighbors[i].0.borrow().processed {
                    // Add the neighbor to the toprocess array
                    toprocess.push(neighbors[i].clone());
                    neighbors[i].0.borrow_mut().processed = true;
                }
            }
        }
    }

    // Return the found cluster
    foundcluster
}

// Find floating clusters
fn findFloatingClusters() -> Vec<Vec<Tile>> {
    withMutLevel(|level| {
        let mut foundclusters = vec![];
        // Reset the processed flags
        resetProcessed(level);

        // Check all tiles
        for i in 0..level.columns {
            for j in 0..level.rows {
                if !level.tiles[i][j].0.borrow().processed {
                    // Find all attached tiles
                    let foundcluster = findCluster(level, i, j, false, false, true);

                    // There must be a tile in the cluster
                    if foundcluster.is_empty() {
                        continue;
                    }

                    // Check if the cluster is floating
                    let mut floating = true;
                    for k in 0..foundcluster.len() {
                        if foundcluster[k].0.borrow().y == 0 {
                            // Tile is attached to the roof
                            floating = false;
                            break;
                        }
                    }

                    if floating {
                        // Found a floating cluster
                        foundclusters.push(foundcluster);
                    }
                }
            }
        }

        foundclusters
    })
}

// Reset the processed flags
fn resetProcessed(level: &mut Level) {
    for i in 0..level.columns {
        for j in 0..level.rows {
            level.tiles[i][j].0.borrow_mut().processed = false;
        }
    }
}

// Reset the removed flags
fn resetRemoved() {
    withMutLevel(|level| {
        for i in 0..level.columns {
            for j in 0..level.rows {
                level.tiles[i][j].0.borrow_mut().removed = false;
            }
        }
    })
}

// Get the neighbors of the specified tile
fn getNeighbors(level: &Level, tile: Tile) -> Vec<Tile> {
    let tile = tile.0.borrow();

    let tilerow = (tile.y + ROWOFFSET.with(|rowoffset| rowoffset.get())) % 2; // Even or odd row
    let mut neighbors = vec![];

    // Get the neighbor offsets for the specified tile
    let n = NEIGHBORSOFFSETS.with(|n| n[tilerow]);

    // Get the neighbors
    for i in 0..n.len() {
        // Neighbor coordinate
        let nx = (tile.x as i32 + n[i][0]) as usize;
        let ny = (tile.y as i32 + n[i][1]) as usize;

        // Make sure the tile is valid
        if nx < level.columns && ny < level.rows {
            neighbors.push(level.tiles[nx][ny].clone());
        }
    }

    neighbors
}

fn updateFps(dt: f64) {
    FPSTIME.with(|fpstime| {
        FPSCOUNT.with(|framecount| {
            FPS.with(|fps| {
                if fpstime.get() > 0.25 {
                    // Calculate fps
                    fps.set((framecount.get() as f64 / fpstime.get()).round());

                    // Reset time and framecount
                    fpstime.set(0.);
                    framecount.set(0);
                }

                // Increase time and framecount
                fpstime.update(|f| f + dt);
                framecount.update(|f| f + 1);
            })
        })
    })
}

// Draw text that is centered
fn drawCenterText(scale: f64, text: &str, x: f64, y: f64, width: f64) {
    CONTEXT.with(|context| {
        let textdim = context.measure_text(text).expect("context measure text");
        context
            .fill_text(
                text,
                x * scale + (width * scale - textdim.width()) / 2.,
                y * scale,
            )
            .expect("context generic fill text");
    })
}

// Render the game
fn render() {
    CONTEXT.with(|context| {
        withLevel(|level| {
            // Draw the frame around the game
            drawFrame(level.scale);

            let yoffset = level.tilesize / 2.;

            // Draw level background
            context.set_fill_style(&JsValue::from_str("#8c8c8c"));
            context.fill_rect(
                level.x - 4. * level.scale,
                level.y - 4. * level.scale,
                level.width + 8. * level.scale,
                level.height + 4. * level.scale - yoffset,
            );

            // Render tiles
            renderTiles(level);

            // Draw level bottom
            context.set_fill_style(&JsValue::from_str("#656565"));
            context.fill_rect(
                level.x - 4. * level.scale,
                level.y - 4. * level.scale + level.height + 4. * level.scale - yoffset,
                level.width + 8. * level.scale,
                2. * level.tilesize + 3. * level.scale,
            );

            // Draw score
            context.set_fill_style(&JsValue::from_str("#ffffff"));
            context.set_font(&format!("{}px Verdana", 18. * level.scale));
            let scorex = level.x + level.width - 150.;
            let scorey = level.y + level.height + level.tilesize - yoffset - 8.;
            drawCenterText(level.scale, "Score:", scorex, scorey, 150.);
            context.set_font(&format!("{}px Verdana", 24. * level.scale));
            drawCenterText(
                level.scale,
                SCORE.with(|s| s.get()).to_string().as_str(),
                scorex,
                scorey + 30.,
                150.,
            );

            if SHOWCLUSTER.with(|showcluster| showcluster.get()) {
                FLOATINGCLUSTERS.with(|floatingclusters| {
                    let floatingclusters = floatingclusters.borrow();
                    if true {
                        use js_sys::Array;
                        use web_sys::console::log;
                        log(&Array::of1(&JsValue::from(&format!(
                            "FloatingClusters {:?}",
                            floatingclusters
                        ))));
                    }
                    // Render cluster
                    CLUSTER
                        .with(|cluster| renderCluster(level, &cluster.borrow(), 255., 128., 128.));

                    for i in 0..floatingclusters.len() {
                        let col = 100. + (100. * i as f64 / floatingclusters.len() as f64).floor();
                        renderCluster(level, &floatingclusters[i], col, col, col);
                    }
                })
            }

            // Render player bubble
            renderPlayer(level);

            // Game Over overlay
            if getGameState() == GameStates::GAMEOVER {
                context.set_fill_style(&JsValue::from_str("rgba(0, 0, 0, 0.8)"));
                context.fill_rect(
                    level.x - 4. * level.scale,
                    level.y - 4. * level.scale,
                    level.width + 8. * level.scale,
                    level.height + 2. * level.tilesize + 8. * level.scale - yoffset,
                );

                context.set_fill_style(&JsValue::from_str("#ffffff"));
                context.set_font(&format!("{}px Verdana", 24. * level.scale));
                drawCenterText(
                    level.scale,
                    "Game Over!",
                    level.x,
                    level.y + level.height / 2. + 10.,
                    level.width,
                );
                drawCenterText(
                    level.scale,
                    "Click to start",
                    level.x,
                    level.y + level.height / 2. + 40.,
                    level.width,
                );
            }
        })
    })
}

// Draw a frame around the game
fn drawFrame(scale: f64) {
    CANVAS.with(|canvas| {
        CONTEXT.with(|context| {
            // Draw background
            context.set_fill_style(&JsValue::from_str("#e8eaec"));
            context.fill_rect(0., 0., canvas.width() as _, canvas.height() as _);

            // Draw header
            context.set_fill_style(&JsValue::from_str("#303030"));
            context.fill_rect(0., 0., canvas.width() as _, 79. * scale);

            // Draw title
            context.set_fill_style(&JsValue::from_str("#ffffff"));
            context.set_font(&format!("{}px Verdana", 24. * scale));
            context
                .fill_text(
                    "Bubble Shooter Example - Rembound.com",
                    10. * scale,
                    37. * scale,
                )
                .expect("context title fill text");

            // Display fps
            context.set_font(&format!("{}px Verdana", 12. * scale));
            context
                .fill_text(
                    &format!("Fps: {}", FPS.with(|f| f.get())),
                    13. * scale,
                    57. * scale,
                )
                .expect("context fps fill text");
        })
    })
}

// Render tiles
fn renderTiles(level: &Level) {
    CONTEXT.with(|context| {
        // Top to bottom
        for j in 0..level.rows as usize {
            for i in 0..level.columns as usize {
                // Get the tile
                let tile = level.tiles[i][j].clone();

                // Get the shift of the tile for animation
                let shift = tile.0.borrow().shift;

                // Calculate the tile coordinates
                let coord = getTileCoordinate(level, i, j);

                // Check if there is a tile present
                if let Some(type_) = tile.clone().0.borrow().type_ {
                    // Support transparency
                    context.save();
                    context.set_global_alpha(tile.0.borrow().alpha);

                    // Draw the tile using the color
                    drawBubble(level.tilesize, coord[0], coord[1] + shift, type_);

                    context.restore();
                }
            }
        }
    })
}

// Render cluster
fn renderCluster(level: &Level, cluster: &Vec<Tile>, r: f64, g: f64, b: f64) {
    CONTEXT.with(|context| {
        for i in 0..cluster.len() {
            // Calculate the tile coordinates
            let coord = getTileCoordinate(level, cluster[i].0.borrow().x, cluster[i].0.borrow().y);

            // Draw the tile using the color
            context.set_fill_style(&JsValue::from_str(&format!("rgb({},{},{})", r, g, b)));
            context.fill_rect(
                coord[0] + level.tilesize / 4.,
                coord[1] + level.tilesize / 4.,
                level.tilesize / 2.,
                level.tilesize / 2.,
            );
        }
    })
}

// Render the player bubble
fn renderPlayer(level: &Level) {
    CONTEXT.with(|context| {
        withPlayer(|player| {
            // Draw player background circle
            context.set_fill_style(&JsValue::from_str("#7a7a7a"));
            context.begin_path();
            context
                .arc_with_anticlockwise(
                    player.x,
                    player.y,
                    level.radius + 12. * level.scale,
                    0.,
                    2. * PI,
                    false,
                )
                .expect("context arc");
            context.fill();
            context.set_line_width(2. * level.scale);
            context.set_stroke_style(&JsValue::from_str("#8c8c8c"));
            context.stroke();

            // Draw the angle
            context.set_line_width(2. * level.scale);
            context.set_stroke_style(&JsValue::from_str("#0000ff"));
            context.begin_path();
            context.move_to(player.x, player.y);
            context.line_to(
                player.x + 1.5 * level.tilesize * player.angle.cos(),
                player.y - 1.5 * level.tilesize * player.angle.sin(),
            );
            context.stroke();

            // Draw the next bubble
            drawBubble(
                level.tilesize,
                player.nextbubble_x - level.tilesize / 2.,
                player.nextbubble_y - level.tilesize / 2.,
                player.nextbubble_tiletype,
            );

            // Draw the bubble
            if player.bubble_visible {
                drawBubble(
                    level.tilesize,
                    player.bubble_x - level.tilesize / 2.,
                    player.bubble_y - level.tilesize / 2.,
                    player.bubble_tiletype,
                );
            }
        })
    })
}

// Get the tile coordinate
fn getTileCoordinate(level: &Level, column: usize, row: usize) -> [f64; 2] {
    let mut tilex = level.x + column as f64 * level.tilesize;

    // X offset for odd or even rows
    if (row + ROWOFFSET.with(|r| r.get())) % 2 != 0 {
        tilex += level.tilesize / 2.;
    }

    let tiley = level.y + row as f64 * level.rowheight;
    [tilex, tiley]
}

// Get the closest grid position
fn getPlayerGridPosition(level: &Level, player: &Player) -> [usize; 2] {
    let gridy = ((player.bubble_y - level.y) / level.rowheight).floor() as usize;

    let xadjust = ((gridy + ROWOFFSET.with(|r| r.get())) % 2) as f64;
    // Check for offset
    let gridx = ((player.bubble_x - xadjust * level.radius - level.x) / level.tilesize).floor() as usize;

    [gridx, gridy]
}

// Draw the bubble
fn drawBubble(tilesize: f64, x: f64, y: f64, index: u8) {
    if index >= BUBBLECOLORS.with(|b| *b) {
        return;
    }

    BUBBLEIMAGE.with(|bubbleimage| {
        let bubbleimage = bubbleimage.borrow();
        if let Some(bubbleimage) = bubbleimage.as_ref() {
            // Draw the bubble sprite
            CONTEXT.with(|context| {
                context
                    .draw_image_with_html_image_element_and_sw_and_sh_and_dx_and_dy_and_dw_and_dh(
                        bubbleimage,
                        index as f64 * 40.,
                        0.,
                        40.,
                        40.,
                        x,
                        y,
                        tilesize,
                        tilesize,
                    )
                    .expect("context draw image");
            })
        }
    })
}

// Start a new game
fn newGame() {
    // Reset score
    SCORE.with(|s| s.set(0));

    TURNCOUNTER.with(|t| t.set(0));
    ROWOFFSET.with(|r| r.set(0));

    // Set the gamestate to ready
    setGameState(GameStates::READY);

    // Create the level
    createLevel();

    // Init the next bubble and set the current bubble
    withLevelMutPlayer(|level, player| {
        nextBubble(level, player);
        nextBubble(level, player);
    })
}

// Create a random level
fn createLevel() {
    withMutLevel(|level| {
        BUBBLECOLORS.with(|bubblecolors| {
            // Create a level with random tiles
            for j in 0..level.rows as usize {
                let mut randomtile = OsRng.gen_range(0, bubblecolors - 1) as u8;
                let mut count = 0;
                for i in 0..level.columns as usize {
                    if count >= 2 {
                        // Change the random tile
                        let mut newtile = OsRng.gen_range(0, bubblecolors - 1) as u8;

                        // Make sure the new tile is different from the previous tile
                        if newtile == randomtile {
                            newtile = (newtile + 1) % bubblecolors;
                        }
                        randomtile = newtile;
                        count = 0;
                    }
                    count += 1;

                    if j < level.rows / 2 {
                        level.tiles[i][j].0.borrow_mut().type_ = Some(randomtile);
                    } else {
                        level.tiles[i][j].0.borrow_mut().type_ = None;
                    }
                }
            }
        })
    })
}

// Create a random bubble for the player
fn nextBubble(level: &Level, player: &mut Player) {
    // Set the current bubble
    player.tiletype = player.nextbubble_tiletype;
    player.bubble_tiletype = player.nextbubble_tiletype;
    player.bubble_x = player.x;
    player.bubble_y = player.y;
    player.bubble_visible = true;

    // Get a random type from the existing colors
    let nextcolor = getExistingColor(level);

    // Set the next bubble
    player.nextbubble_tiletype = nextcolor;
}

// Get a random existing color
fn getExistingColor(level: &Level) -> u8 {
    let existingcolors = findColors(level);

    if existingcolors.len() > 0 {
        existingcolors[OsRng.gen_range(0, existingcolors.len() - 1)]
    } else {
        0
    }
}

// Shoot the bubble
fn shootBubble() {
    withMutPlayer(|player| {
        if false {
            use js_sys::Array;
            use web_sys::console::log;
            log(&Array::of1(&JsValue::from(&format!("Pre:  {:?}", player))));
        }

        // Shoot the bubble in the direction of the mouse
        player.bubble_x = player.x;
        player.bubble_y = player.y;
        player.bubble_angle = player.angle;
        player.bubble_tiletype = player.tiletype;

        if false {
            use js_sys::Array;
            use web_sys::console::log;
            log(&Array::of1(&JsValue::from(&format!("Post: {:?}", player))));
        }

        // Set the gamestate
        setGameState(GameStates::SHOOTBUBBLE);
    })
}

// On mouse movement
fn onMouseMove(e: MouseEvent) {
    withMutPlayer(|player| {
        // Get the mouse position
        let pos = getMousePos(e);

        // Get the mouse angle
        let mouseangle = (player.y as f64 - pos[1])
            .atan2(pos[0] - player.x as f64);

        // Restrict angle to 8, 172 degrees
        const LBOUND: f64 = 0.13962634015954636; // 8
        const UBOUND: f64 = 3.001966313430247; // 172

        // Set the player angle
        player.angle = num::clamp(mouseangle - PI * (mouseangle / PI).floor(), LBOUND, UBOUND);
    })
}

// On mouse button click
fn onMouseDown(e: MouseEvent) {
    onMouseMove(e);

    match getGameState() {
        GameStates::READY => shootBubble(),
        GameStates::GAMEOVER => newGame(),
        _ => {}
    }
}

// On touch movement
fn onTouchMove(e: Touch) {
    withMutPlayer(|player| {
        // Get the mouse position
        let pos = getTouchPos(e);

        // Get the mouse angle
        let mouseangle = (player.y as f64 - pos[1])
            .atan2(pos[0] - player.x as f64);

        // Restrict angle to 8, 172 degrees
        const LBOUND: f64 = 0.13962634015954636; // 8
        const UBOUND: f64 = 3.001966313430247; // 172

        // Set the player angle
        player.angle = num::clamp(mouseangle - PI * (mouseangle / PI).floor(), LBOUND, UBOUND);
    })
}

// On Touch Start
fn touchHandleStart(e: TouchEvent) {
    e.prevent_default();
    if let Some(targetTouch) = e.target_touches().get(0) {
        onTouchMove(targetTouch);
    }
}

// On Touch Move
fn touchHandleMove(e: TouchEvent) {
    e.prevent_default();
    if let Some(targetTouch) = e.target_touches().get(0) {
        onTouchMove(targetTouch);
    }
}

// On Touch End
fn touchHandleEnd(e: TouchEvent) {
    e.prevent_default();

    match getGameState() {
        GameStates::READY => shootBubble(),
        GameStates::GAMEOVER => newGame(),
        _ => {}
    }
}

// On Touch Cancel
fn touchHandleCancel(e: TouchEvent) {
    e.prevent_default();
}

// Get the mouse position
fn getMousePos(e: MouseEvent) -> [f64; 2] {
    CANVAS.with(|canvas| {
        withLevel(|level| {
            let rect = canvas.get_bounding_client_rect();
            [
                (canvas.width() as f64 * (e.client_x() as f64 - rect.left()))
                    / (level.dpr * (rect.right() - rect.left())),
                (canvas.height() as f64 * (e.client_y() as f64 - rect.top()))
                    / (level.dpr * (rect.bottom() - rect.top())),
            ]
        })
    })
}

fn getTouchPos(e: Touch) -> [f64; 2] {
    CANVAS.with(|canvas| {
        withLevel(|level| {
            let rect = canvas.get_bounding_client_rect();
            [
                (canvas.width() as f64 * (e.client_x() as f64 - rect.left()))
                    / (level.dpr * (rect.right() - rect.left())),
                (canvas.height() as f64 * (e.client_y() as f64 - rect.top()))
                    / (level.dpr * (rect.bottom() - rect.top())),
            ]
        })
    })
}
